This repository is a collection of ideas I or other people had and which I don't 
want to forget. Part of collecting these ideas publicly is to ensure that
students know that I would be interested in supervising projects in this area.


## Open Thesis Topics (Master or Bachelor level)
If you are interested in one of the topics below, please email me 
(hseibold [at] ibe.med.uni-munechen.de) or open an 
[issue](https://gitlab.com/research_hs/research_ideas/issues).


### Pruning in Model-Based Trees
Model-based trees such as LM trees and GLM trees are usually pre-pruned, which 
means we have an early stopping criterion based on p-values. It is possible, 
however, to grow large trees and **post-prune** them. **AIC and BIC** are intuitive
measures that can be used in post-pruning. Which of them is better and how the
performance compares to pre-pruning is so far unclear and would make an 
exciting thesis topic.

Reading:
- model-based trees: [vignette](https://cran.r-project.org/web/packages/partykit/vignettes/mob.pdf), 
[paper](https://www.tandfonline.com/doi/abs/10.1198/106186008X319331)

### Comparing Random Forest Methods for Personalized Medicine
The R packages **model4you** and **grf** both allow for personalized treatment effect 
estimation. A possible thesis would be to compare the two packages in methodology
and performance.

Reading:
- grf: [repository](https://github.com/swager/grf), [paper](https://arxiv.org/abs/1610.01271)
- model4you: [website](https://model4you.gitlab.io/), 
[stratified medicine paper](http://journals.sagepub.com/doi/10.1177/0962280217693034), 
[personalised medicine paper](http://journals.sagepub.com/doi/10.1177/0962280217693034)

### Estimating Optimal Treatment Rules from Observational Data
Optimal Treatment Rules (OTRs) are decision rules that say which treatment is the 
best for a given patient. Obtaining OTRs is particularly challenging when the
treatment is not randomly assigned to patients but was assigned by the doctor
in charge, because **causal relations** are not clear any longer. A thesis in this
field could (a) compare the perfomance of different methods for estimating OTRs in 
observational data or (b) extend model-based trees in a way that they can handle
observational data and integrate causal beliefs. For (b) prior experience in
causal inference would be neccessary.

- model-based trees: [vignette](https://cran.r-project.org/web/packages/partykit/vignettes/mob.pdf), 
[paper](https://www.tandfonline.com/doi/abs/10.1198/106186008X319331)
- causal inference: [best practices paper](http://doi.wiley.com/10.1002/sim.6607)

*UPDATE:* Topic (a) is already taken.


### Causal model-based forest
In each tree of a forest we could use a matched subsample of the original data.
Would this lead to an unbiased causal effect estimate?


### Extracting rules from trees

Trees as in **rpart** or **partykit** define (decision) rules. It would be nice to
have a well working method for extracting rules from many types of trees in **R**.
In **partykit** a hidden function exists but needs to be improvement
(`partykit:::.list.rules.party()`).

Reading:
- https://stackoverflow.com/questions/29999626/how-to-extract-the-splitting-rules-for-the-terminal-nodes-of-ctree
- https://github.com/gluc/data.tree/issues/126

